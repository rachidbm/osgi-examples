package net.luminis.greeter.managedservice;

import net.luminis.greeter.api.Greeter;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ManagedService;

import java.util.Dictionary;
import java.util.Hashtable;

public class Activator implements BundleActivator {

    private ServiceRegistration<?> serviceRegistration;

    @Override
    public void start(BundleContext context) {
        Dictionary<String, Object> properties = new Hashtable<>();
        properties.put(Constants.SERVICE_PID, "net.luminis.greeter.managedservice");
        serviceRegistration = context.registerService(new String[]{
                Greeter.class.getName(),
                ManagedService.class.getName()
        }, new GreeterImpl(), properties);
    }

    @Override
    public void stop(BundleContext context) {
        serviceRegistration.unregister();
    }
}
