package net.luminis.greeter.managedcomponent;

import net.luminis.greeter.api.Greeter;
import org.osgi.service.cm.annotations.RequireConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;

@RequireConfigurationAdmin
@Component(
        configurationPid = "net.luminis.greeter.managedcomponent",
        configurationPolicy = ConfigurationPolicy.REQUIRE,
        immediate = true
)
public class GreeterComponent implements Greeter {

    @interface GreeterComponentConfig {
        String greeting();
    }

    private String greeting;

    @Activate
    public GreeterComponent(GreeterComponentConfig config) {
        this.greeting = config.greeting();
        System.out.println("Activate GreeterComponent with greeting: '" + greeting + "'");
    }

    @Modified
    public void updateConfiguration(GreeterComponentConfig config) {
        this.greeting = config.greeting();
        System.out.println("Update GreeterComponent greeting to: '" + greeting + "'");
    }

    @Override
    public void sayHi() {
        System.out.println(greeting);
    }


}
