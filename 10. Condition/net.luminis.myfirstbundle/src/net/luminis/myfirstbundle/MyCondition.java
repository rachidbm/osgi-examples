package net.luminis.myfirstbundle;

import net.luminis.person.api.Person;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.condition.Condition;

@Component(property = {
        "osgi.condition.id=my.condition"
})
public class MyCondition implements Condition {

    @Reference(target = "(author=Xander)")
    Person person;
}
