package net.luminis.util;

import org.h2.tools.Server;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jpa.EntityManagerFactoryBuilder;

import java.sql.SQLException;

@Component(immediate = true)
public class H2ServerLauncher {

    @Activate
    public H2ServerLauncher(
            @Reference(target = "(osgi.unit.name=todo)") EntityManagerFactoryBuilder entityManagerFactoryBuilder
    ) {
        try {
            Server webServer;
            webServer = Server.createWebServer("-web", "-webPort", "8082");
            webServer.start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
