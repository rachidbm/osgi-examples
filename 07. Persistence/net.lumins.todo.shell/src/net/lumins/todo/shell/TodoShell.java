package net.lumins.todo.shell;

import net.luminis.todo.api.Todo;
import net.luminis.todo.api.TodoService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.List;

@Component(property = {
    "osgi.command.scope=todo",
    "osgi.command.function=add",
    "osgi.command.function=complete",
    "osgi.command.function=todo"
}, service = Object.class)
public class TodoShell {

    private final TodoService todoService;

    @Activate
    public TodoShell(@Reference TodoService todoService) {
        this.todoService = todoService;
    }

    public Todo add(String description) {
        return todoService.add(description);
    }

    public Todo complete(Long  id) {
        return todoService.complete(id);
    }

    public List<Todo> todo() {
        return todoService.list();
    }



}
