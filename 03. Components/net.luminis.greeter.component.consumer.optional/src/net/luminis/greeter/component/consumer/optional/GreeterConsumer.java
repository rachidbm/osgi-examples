package net.luminis.greeter.component.consumer.optional;


import net.luminis.greeter.api.Greeter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

@Component
public class GreeterConsumer {

    private Greeter greeter;

    @Activate
    public GreeterConsumer() {
        System.out.println("Greeter consumer activated");
    }

    @Reference(policy = ReferencePolicy.DYNAMIC,  cardinality = ReferenceCardinality.OPTIONAL)
    public void bindGreeter(Greeter greeter) {
        this.greeter = greeter;
        System.out.println("Greeter bound");
        greeter.sayHi();
    }

    public void unbindGreeter(Greeter greeter) {
        System.out.println("Greeter unbound");
        greeter.sayHi();
        this.greeter = null;
    }

    @Deactivate
    public void stop() {
        System.out.println("Greeter consumer deactivated");

    }

}
