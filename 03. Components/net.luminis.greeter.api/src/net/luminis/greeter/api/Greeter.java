package net.luminis.greeter.api;

import org.osgi.annotation.versioning.ProviderType;

@ProviderType
public interface Greeter {

    void sayHi();

}
