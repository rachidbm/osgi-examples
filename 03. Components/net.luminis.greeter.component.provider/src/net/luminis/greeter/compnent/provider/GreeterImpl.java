package net.luminis.greeter.compnent.provider;

import net.luminis.greeter.api.Greeter;
import org.osgi.service.component.annotations.Component;

@Component
public class GreeterImpl implements Greeter {

    @Override
    public void sayHi() {
        System.out.println("Hi!");
    }
}
