package net.luminis.greeting.consumer;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

// Change the topics to greeting/nl OR greeting/* to get a different language of in all languages
@Component(property = { "event.topics=net/luminis/greeting/en", "event.topics=net/luminis/greeting/nl" })
public class GreetingConsumer implements EventHandler {

    @Override
    public void handleEvent(Event event) {
        System.out.println( event.getProperty("greeting"));
    }
}
