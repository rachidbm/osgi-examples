package net.luminis.greeter.provider.dutch;

import net.luminis.greeter.api.Greeter;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceRegistration;

import java.util.Dictionary;
import java.util.Hashtable;

public class Activator implements BundleActivator {

    private volatile ServiceRegistration<Greeter> greeterServiceRegistration;

    @Override
    public void start(BundleContext context) {

        Dictionary<String, Object> properties = new Hashtable<>();
        properties.put(Constants.SERVICE_RANKING, 1);

        greeterServiceRegistration = context.registerService(Greeter.class, new DutchGreeter(), properties);
    }

    @Override
    public void stop(BundleContext context) {
        greeterServiceRegistration.unregister();
    }
}
