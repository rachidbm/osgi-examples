package net.luminis.greeter.provider.english;


import net.luminis.greeter.api.Greeter;

public class EnglishGreeter implements Greeter {

    @Override
    public void sayHi() {
        System.out.println("Hi!");
    }

}
