package net.luminis.greeter.consumer;

import net.luminis.greeter.api.Greeter;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class Activator implements BundleActivator {
    @Override
    public void start(BundleContext context) {
        ServiceReference<Greeter> serviceReference = context.getServiceReference(Greeter.class);

        Greeter greeter = context.getService(serviceReference);

        greeter.sayHi();


        // disable the ungetService call to demonstrate usage tracking
        // use:
        // inspect cap service 2
        // to see where the greeter service is used

        context.ungetService(serviceReference);
    }

    @Override
    public void stop(BundleContext context) {

    }
}
