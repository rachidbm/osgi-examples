package net.luminis.greeter.repeating.consumer;

import net.luminis.greeter.api.Greeter;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class Activator implements BundleActivator {

    private volatile boolean started;
    private Thread greeterThread;

    @Override
    public void start(BundleContext context) {
        started = true;
        greeterThread = new Thread(() -> {

            while (started) {
                try {
                    ServiceReference<Greeter> serviceReference = context.getServiceReference(Greeter.class);
                    Greeter greeter = context.getService(serviceReference);

                    greeter.sayHi();

                    context.ungetService(serviceReference);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {

                }
            }

        });

        greeterThread.start();
    }

    @Override
    public void stop(BundleContext context) throws InterruptedException {
        started = false;
        greeterThread.join();
    }
}
