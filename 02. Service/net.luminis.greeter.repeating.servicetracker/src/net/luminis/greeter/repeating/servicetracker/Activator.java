package net.luminis.greeter.repeating.servicetracker;

import net.luminis.greeter.api.Greeter;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class Activator implements BundleActivator {

    private volatile boolean started;
    private Thread greeterThread;

    @Override
    public void start(BundleContext context) {
        started = true;
        greeterThread = new Thread(() -> {
            ServiceTracker<Greeter, Greeter> serviceTracker = new ServiceTracker<>(context, Greeter.class, null);
            serviceTracker.open();

            while (started) {
                try {
                    Greeter greeter = serviceTracker.getService();

                    greeter.sayHi();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                }
            }

            serviceTracker.close();
        });

        greeterThread.start();
    }

    @Override
    public void stop(BundleContext context) throws InterruptedException {
        started = false;
        greeterThread.join();
    }
}
