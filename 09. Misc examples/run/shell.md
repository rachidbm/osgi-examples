# Useful shell commands

## Requirements and capabilities

### Bundle package requirements

`inspect c osgi.wiring.package 1`

### All bundle capabilities

`inspect c \* 1`