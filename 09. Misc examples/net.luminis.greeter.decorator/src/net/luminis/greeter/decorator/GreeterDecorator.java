package net.luminis.greeter.decorator;

import net.luminis.greeter.api.Greeter;

public class GreeterDecorator {

    public void sayDecoratedHi(Greeter greeter) {
        System.out.print("Decorated ");
        greeter.sayHi();
    }
}
