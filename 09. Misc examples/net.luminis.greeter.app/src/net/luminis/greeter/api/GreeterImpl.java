package net.luminis.greeter.api;

public class GreeterImpl implements Greeter{

    @Override
    public void sayHi() {
        System.out.println("Hi from version 1.0");
    }
}
