package com.acme.foo;

public interface Foo {

    default String getVersion() {
        return "1.0 (from B)";
    }

}
