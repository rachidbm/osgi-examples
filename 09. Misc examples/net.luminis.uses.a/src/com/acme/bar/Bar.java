package com.acme.bar;

// import com.acme.foo.Foo;

public interface Bar {

    // Making A expose Foo as part of its API will cause issues in the dependency chain
    // Foo getTheFoo();
    void getFoo();

    default String getVersion() {
        return "1.0 (from A)";
    }
}
