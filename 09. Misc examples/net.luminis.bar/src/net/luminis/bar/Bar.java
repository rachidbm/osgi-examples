package net.luminis.bar;

import net.luminis.foo.Foo;

public interface Bar {

    Foo getFoo();

}
